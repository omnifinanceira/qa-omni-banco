require 'date'
require 'fileutils'
require 'rubygems'

module Helper
   def tirar_foto(nome_arquivo, resultado)
      caminho_arquivo = "results/screenshots/test_#{resultado}"
      foto = "#{caminho_arquivo}/#{nome_arquivo}.png"
      page.save_screenshot(foto)
      attach(page.save_screenshot(foto), 'image/png')
   end

   def verificar_url
      if current_url.include? CONFIG['url_padrao']
      else
          binding.pry
      end
  end
end