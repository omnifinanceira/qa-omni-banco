Before do
    # $driver.start_driver
end

After do |scenario|
    $driver.quit_driver

    scenario_name = scenario.name.gsub(/\s+/,'_').tr('/','_')

    if scenario.failed?
     tirar_foto(scenario_name.downcase!, 'falhou')
    else
     tirar_foto(scenario_name.downcase!, 'passou')
    end
end

AfterStep do
    verificar_url
end