class SQLConnect

    def select(query)
        connectionstring = OCI8.new(BANCO["username"], BANCO["password"], BANCO["dbname"])
        result = []
        parser = connectionstring.parse(query)
        parser.exec
        parser.fetch() { |row| result << row[0].to_i }
        return result
    end
end