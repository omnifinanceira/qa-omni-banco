require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require 'capybara'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'appium_capybara'
require 'appium_lib'
require 'pry'
require 'oci8'
require 'simple_xlsx_reader'
require 'RubyXL'
require 'cpf_cnpj'
require 'faker'
require_relative 'helper.rb'
require_relative '../pages/util.rb'
require_relative '../support/DAO/SQLConnect.rb'
require_relative '../support/DAO/Queries.rb'

BROWSER = ENV['BROWSER']
AMBIENTE = ENV['AMBIENTE']
CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/#{AMBIENTE}.yml")
BANCO = YAML.load_file(File.dirname(__FILE__) + "/DAO/config.yml")

World(Helper)
World(Capybara::DSL)
World(Capybara::RSpecMatchers)

util = Util.new
util.criar_pasta_log

Capybara.register_driver :selenium do |app|
    if BROWSER.eql?('chrome')
        @caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['start-maximized']})
    elsif BROWSER.eql?('chrome_headless')
        @caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['--headless']})
    end

    Capybara::Selenium::Driver.new(app, {:browser => :chrome, :desired_capabilities => @caps})
end

Capybara.configure do |config|
    config.default_driver = :selenium
    config.app_host = CONFIG['url_padrao']
    config.default_max_wait_time = 5
end

def caps
{   caps: {
        deviceName: "emulator-5554",
        platformName: "Android",
        noReset: "true",
        fullReset: "false",
        appActivity: "br.com.omni.omni.view.access.SplashActivity",
        automationName: "UIAutomator2",
        appPackage: "br.com.omni.omni",
        newCommandTimeout: 3600,
    }
}
end

Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object