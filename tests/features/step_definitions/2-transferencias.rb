Dado('acesso a transferencias') do
    home = Home.new
    home.acessar_transferencias
    @transferencias = Transferencias.new
end

Quando('seleciono o tipo de transferencia {string}') do |tipo_transferencia|
    @transferencias.acessar_outros_bancos
    @transferencias.selecionar_tipo_transferencia(tipo_transferencia)
end

Quando('preencho os dados do favorecido {string},  {string}, {string}, {string}, {string} e {string}') do |banco, modalidade, agencia, conta, titularidade,favorecido|
    favorecido == "true" ? favorecido = true : favorecido = false
    # expect(page).to have_selector(:xpath, "//div/mat-tab-body/div/form/div[@class='form-holder']", visible: true)
    @transferencias.preencher_dados_favorecidos(banco,modalidade,agencia,conta,titularidade,favorecido)
end

Quando('preencho os dados da transferencia {string} e {string}') do |valor,agendamento|
    @agendamento = agendamento
    agendamento == "true" ? agendamento = true : agendamento = false
    @valoratualizado = @transferencias.preencher_valor(valor)
    @transferencias.selecionar_data(agendamento)
end

Entao('valido as informacoes a serem efetivadas {string},  {string}, {string}, {string} e  {string}') do |banco, modalidade, agencia, conta, titularidade|
    textotranferencia =  all(:xpath, "//div[@class='item-confirmation']/strong")
        expect((banco.upcase).match(textotranferencia[0].text.upcase)).to be_truthy
        expect(agencia.match(textotranferencia[1].text)).to be_truthy
        expect(conta.match(textotranferencia[2].text.gsub('-',''))).to be_truthy
        expect(titularidade.match(textotranferencia[3].text)).to be_truthy
        expect(modalidade.match(textotranferencia[6].text)).to be_truthy
        expect(@valoratualizado.match(textotranferencia[9].text.gsub('R$ ','').gsub(',00','').gsub('.',''))).to be_truthy
end

Quando('eu transfiro entre {string} omni') do |conta|
    @conta = conta
    @transferencias.acessar_entre_bancos
    @transferencias.transferir_entre_contas(conta,true)
end

Quando('autentico') do
    $driver.start_driver
    @token = Token.new
    @token.preencher_assinatura_eletronica
    @token.clicar_proximo
    @token.acessar_mobile
    @token.clicar_token
    token = @token.pegar_token
    # t = @token.pegar_token_txt
    @token.preencher_token(token)

    @token.clicar_finalizar
end

Entao('valido se a transferencia entre conta omni foi realizada') do
    textosucesso =  find(:xpath, "//div[@data-component='transfer-result']/app-warning/div/p")

    if @agendamento
        expect(/Transferência agendada com sucesso!/.match(textosucesso.text)).to be_truthy
    else
        expect(/Transferência realizada com sucesso!/.match(textosucesso.text)).to be_truthy
    end

    textotranferencia =  all(:xpath, "//div[@class='item-confirmation']/strong")
        expect(@conta.match(textotranferencia[1].text.gsub('-',''))).to be_truthy
        expect(@valoratualizado.match(textotranferencia[5].text.gsub('R$ ','').gsub(',00','').gsub('.',''))).to be_truthy
end

Entao('valido se a transferencia foi realizada') do
    textosucesso =  find(:xpath, "//div[@data-component='warning']/p")
    if @agendamento == true
        expect(/Transferência agendada com sucesso!/.match(textosucesso.text)).to be_truthy
    else
        expect(/Transferência realizada com sucesso!/.match(textosucesso.text)).to be_truthy
    end
end

Entao('valido se o favorecido foi adicionado') do
    textosucesso =  find(:xpath, "//div[@data-component='warning']/p")
        expect(/Favorecido incluido com sucesso!/.match(textosucesso.text)).to be_truthy
end

Quando('acesso favorecidos') do
    @transferencias.acessar_favorecidos
end

Quando('clico em adicionar favorecido') do
    expect(page).to have_selector(:xpath, "//div[@class='steps-container']/app-list-benefactors/div/div/mat-table", visible: true, wait: 5)
    @transferencias.adicionar_favorecido
end