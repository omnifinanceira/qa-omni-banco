Quando('eu acesso a pagina de investimentos') do
    home = Home.new
    home.acessar_investimentos
    @investimento = Investimentos.new
end

Quando('que realizo uma simulacao') do
    @investimento.realizar_simulacao
end

Entao('valido se e possivel efetivar o investimento') do
    textodadosinvestimento =  all(:xpath, "//div[@class='list-confirmation']/div/strong")
    textodadosinvestimento.each {|n| expect(n.text).not_to be_empty}
end

Quando('que acesso o submenu de extrato') do
    @investimento.acessar_extrato
end

Entao('valido os valores investidos') do
   #ainda não implementado validação
end

Quando('que acesso o submenu de por periodo') do
    @investimento.acessar_por_periodo
end

Entao('valido os valores investidos por periodo') do
    #ainda não implementado validação
end

Quando('que acesso o submenu resgate') do
    @investimento.acessar_resgatar
end

Quando('realizo o resgate') do
    #ainda não implementado resgate
end

Entao('valido se o resgate foi efetuado') do
    #ainda não implementado validação
end