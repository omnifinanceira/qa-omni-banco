Quando('eu preencho o codigo de barras') do 
    home = Home.new
    home.acessar_pagamentos
    @codigodebarras = Util.new
    numerocodigodebarras = @codigodebarras.pegar_codigo_de_barras
    log(numerocodigodebarras)
    @pagamentos = Pagamentos.new
    @pagamentos.preencher_codigo_de_barras(numerocodigodebarras)
end

Quando('seleciono a opcao para pagar hoje ou {string}') do |agendo|
    agendo == "true" ? agendo = true : agendo = false
    @agendado = agendo
    @pagamentos.selecionar_data(agendo)
end

Entao('valido se o pagamento foi efetuado') do
    if @agendado
        textoconfirmacaoboleto = find(:xpath, "//app-warning/div/p")
            expect(/Pagamento agendado com sucesso!/ .match(textoconfirmacaoboleto.text)).to be_truthy
    else
        textoconfirmacaoboleto = find(:xpath, "//app-warning/div/p")
            expect(/Pagamento realizado com sucesso!/ .match(textoconfirmacaoboleto.text)).to be_truthy
    end

    @codigodebarras.apagar_codigo_de_barras
end