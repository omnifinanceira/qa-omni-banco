Quando('eu acesso a pagina de cobranca') do
    home = Home.new
    home.acessar_cobranca
end

Entao('valido os campos da tabela de sacados') do
    textotitulomenu =  all(:xpath, "//mat-header-row/mat-header-cell")
        expect('Nome / Razão Social'.match(textotitulomenu[0].text)).to be_truthy
        expect(/E-mail/.match(textotitulomenu[1].text)).to be_truthy
        expect('CPF / CNPJ'.match(textotitulomenu[2].text)).to be_truthy

    qtdcadastrossacado = all(:xpath, "//mat-table/mat-row")
        expect(qtdcadastrossacado.count).to be >  0
end

Quando('eu acesso o cadastro de novo sacado') do
    @cobranca = Cobranca.new
    @cobranca.acessar_sacado
end

Entao('eu cadastro um sacado pessoa fisica') do
    @cobranca.cadastrar_sacado
end

Entao('eu cadastro um sacado pessoa juridica') do
    @cobranca.cadastrar_sacado(true)
end

Entao('valido se o cadastro foi efetuado') do
    textosucesso =  find(:xpath, "//div/app-warning/div/p")
        expect(/Sacado cadastrado com sucesso!/ .match(textosucesso.text)).to be_truthy

    expect(page.has_xpath?("//button/span[text()='Emitir Boleto']")).to eq true
end

Entao('clico em emitir boleto') do
    @cobranca.clicar_emitir_boleto
end

Entao('preencho valor e data de vencimento') do
    @valorboleto = @cobranca.preencher_valor
    @dia = @cobranca.selecionar_data
    @cobranca.clicar_confirmar
end

Entao('informo a assinatura eletronica') do
    token = Token.new
    token.preencher_assinatura_eletronica
    @cobranca.clicar_gerar_boleto
end

Entao('verifico se o boleto foi gerado') do
    textoconfirmacaoboleto = find(:xpath, "//div[@data-component='warning']/p")
      expect(/Boleto gerado com sucesso!/ .match(textoconfirmacaoboleto.text)).to be_truthy

    textoatencao = find(:xpath, "//div[@class='info-header']/p")
      expect("Atenção! O boleto estará disponível para pagamento em 30 minutos." .match(textoatencao.text)).to be_truthy 

    textodadosboleto = all(:xpath, "//div[@class='list-confirmation']/div/span[@class='right']")
      expect(@valorboleto.match(textodadosboleto[0].text.gsub("R$ ","").gsub(".",""))).to be_truthy
      @dia.to_s.length == 1 ? dia_s = "0" + @dia.to_s : dia_s = @dia.to_s
      expect(dia_s.match(textodadosboleto[1].text.split("/")[0])).to be_truthy
    
    #verificar botao imprimir e PDF
    expect(page.has_xpath?("//button[@class='btn-export']/span[text()='Imprimir']")).to eq true
    expect(page.has_xpath?("//button[@class='btn-export']/span[text()='PDF']")).to eq true
end