Dado('que eu esteja logado') do
    @login = Login.new
    @login.load
    @login.acessar_conta_ib
end

Quando('valido os textos dos campos existentes no menu lateral') do
    textomenulateral = all(:xpath, "//ul[@class='list-menu']/li/a/span")
        expect(/Página inicial/.match(textomenulateral[0].text)).to be_truthy
        expect(/Transferências/.match(textomenulateral[1].text)).to be_truthy
        expect(/Pagamentos/.match(textomenulateral[2].text)).to be_truthy
        expect(/Extrato de conta/.match(textomenulateral[3].text)).to be_truthy
        expect(/Cobrança/.match(textomenulateral[4].text)).to be_truthy
        expect(/Depósitos/.match(textomenulateral[5].text)).to be_truthy
        expect(/Investimentos/.match(textomenulateral[6].text)).to be_truthy
        expect(/Transações/.match(textomenulateral[7].text)).to be_truthy
        expect(/Comprovantes/.match(textomenulateral[8].text)).to be_truthy
        expect(/Minha conta/.match(textomenulateral[9].text)).to be_truthy
end

Quando('valido os textos dos campos existentes no balance') do
    textobalance = all(:xpath, "//div[@class='balance-box']/div/p[not(@class='price')]/strong")
    expect(/Saldo Atual/.match(textobalance[0].text)).to be_truthy
    expect(/Limite Contratado/.match(textobalance[1].text)).to be_truthy
    # expect(/#{'Saldo Total'}/.match(textobalance[2].text)).to be_truthy
    expect(textobalance[2].text).to eq("Saldo Total Disponível (Saldo atual + Limite Contratado)")
end

Quando('valido os textos dos campos existentes no lancamento') do
    textolancamento = all(:xpath, "//mat-header-row/mat-header-cell")
    expect(/Data/.match(textolancamento[0].text)).to be_truthy
    expect(/Histórico/.match(textolancamento[1].text)).to be_truthy
    expect(/Valor/.match(textolancamento[2].text)).to be_truthy

    # expect(/Saldo/.match(textolancamento[3].text)).to be_truthy
end

Quando('valido o link de evolucao da sua conta e pagamentos de codigo de barras') do
    #evolução da sua conta
    # expect(page.has_xpath?("//div[@data-component='account-evolution']/div[@class='box']")).to eq true

    #Pagamento com código de barras
    expect(page.has_xpath?("//div[@class='applications']/app-bar-code/div[@class='box']")).to eq true
end

Entao('valido os textos dos campos existentes nos botoes inferiores') do  
    textobotoesinferiores = all(:xpath, "//div[@data-component='account-options']/div/div/a/span")
        # expect(/#{'Renomear Conta'}/.match(textobotoesinferiores[0].text)).to be_truthy
        # expect(/#{'Outros Bancos'}/.match(textobotoesinferiores[1].text)).to be_truthy
        # expect(/#{'Simulação'}/.match(textobotoesinferiores[2].text)).to be_truthy
        expect(textobotoesinferiores[0].text).to eq("Minha Conta / Renomear Conta")
        expect(textobotoesinferiores[1].text).to eq("Transferências / Outros Bancos")
        expect(textobotoesinferiores[2].text).to eq("Investimentos / Simulação")
        expect(/Comprovantes/.match(textobotoesinferiores[3].text)).to be_truthy
end