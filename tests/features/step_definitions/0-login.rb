Quando('eu preencher o campo {string}') do |cpf|
  @login = Login.new
  @login.load
  @login.preencher_cpf(cpf)
end

Quando('eu clicar no botao proximo') do
  @login.clicar_botao_proximo
end

Quando('eu digitar a {string}') do |senha|
 @login.preencher_senha(senha)
end

Quando('eu clicar no botao Entrar') do
  @login.acessar_conta
end

Entao('eu espero visualizar a home do internet banking') do
  textohomeextrato = find(:xpath, "//app-main-title/div/p[@class='subtitle']")
    expect('Confira aqui um resumo do seu extrato, investimentos e crédito'.match(textohomeextrato.text)).to be_truthy
end

Entao('verifico a {string} de retorno') do |mensagem|
  textohomeextrato = find(:xpath, "//snack-bar-container/simple-snack-bar")
    expect(mensagem.match(textohomeextrato.text)).to be_truthy
end