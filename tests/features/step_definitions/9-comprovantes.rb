Dado('que verifico o menu de meses') do
    home = Home.new
    home.acessar_comprovantes
    
    qtdmeses = all(:xpath, "//div[@class='slick-list']/div/div")
        expect(qtdmeses.count == 12)
end

Dado('o menu de periodo') do
    textomenuperiodo = all(:xpath, "//div[@data-component='special-filter']/div[@class='filter-holder']/div/button/span")
        expect(/Hoje/.match(textomenuperiodo[0].text)).to be_truthy
        expect(/15 dias/.match(textomenuperiodo[1].text)).to be_truthy
        expect(/30 dias/.match(textomenuperiodo[2].text)).to be_truthy
        expect(/Período/.match(textomenuperiodo[3].text)).to be_truthy
end

Entao('verifico a tabela de comprovantes') do
    textotabelacomprovantes = all(:xpath, "//div[@class='table-head non-printable']/div/strong")
        expect(/Data/.match(textotabelacomprovantes[0].text)).to be_truthy
        expect(/Descrição/.match(textotabelacomprovantes[1].text)).to be_truthy
        expect(/Valor/.match(textotabelacomprovantes[2].text)).to be_truthy 
end