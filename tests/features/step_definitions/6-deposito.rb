Quando('eu acesso a pagina de deposito') do
    home = Home.new
    home.acessar_deposito
end

Quando('valido as sugestoes de valor para deposito') do
    textosugestaodevalor =  all(:xpath, "//div/div[@class='add-values']/span")

    valoresdatela = textosugestaodevalor.map {|n| n.text}
        expect(valoresdatela).to include('+R$ 5,00','+R$ 10,00', '+R$ 50,00', '+R$ 100,00')
end

Entao('realizo o deposito via boleto') do
   deposito = Deposito.new
   deposito.realizar_deposito
end

Entao('valido se o deposito foi efetuado') do
    textoboleto = find(:xpath, "//div[@data-component='warning']/p" )
        expect(/Boleto gerado com sucesso!/.match(textoboleto.text)).to be_truthy
end