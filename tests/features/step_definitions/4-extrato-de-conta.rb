Quando('eu acesso a pagina de extrato de conta') do
    home = Home.new
    home.acessar_extrato_de_conta
end

Entao('valido todos os campos') do
    qtdmesesexibidos = all(:xpath, "//div[@class='slick-track']/div")
        expect(qtdmesesexibidos.count == 12)

    textofiltrodata =  all(:xpath, "//div[@class='filter-holder']/div/button/span")
        expect(/Hoje/.match(textofiltrodata[0].text)).to be_truthy
        expect(/15 dias/.match(textofiltrodata[1].text)).to be_truthy
        expect(/30 dias/.match(textofiltrodata[2].text)).to be_truthy
        expect(/Período/.match(textofiltrodata[3].text)).to be_truthy

    textotabelaextrato = all(:xpath, "//mat-header-row/mat-header-cell")
        expect(/Data/.match(textotabelaextrato[0].text)).to be_truthy
        expect(/Histórico/.match(textotabelaextrato[1].text)).to be_truthy
        expect(/Valor/.match(textotabelaextrato[2].text)).to be_truthy

        # expect(/Saldo/.match(textotabelaextrato[3].text)).to be_truthy
end