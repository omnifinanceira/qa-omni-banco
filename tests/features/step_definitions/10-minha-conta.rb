Dado('acesso a pagina minha conta') do
        home = Home.new
        home.acessar_minha_conta

        @minhaconta = MinhaConta.new
end

Dado('que verifico o limite para transferencia') do
    expect(page.has_xpath?("//div[@class='data-row']/h2/strong[text()='Transferências']")).to eq true
    
    @textinformacaominhaconta = all(:xpath, "//div[@class='data-row']/div")
        # expect(@textinformacaominhaconta[0].text).to include('DOC', 'Máximo', 'individual:', 'R$ 4.999,99', 'Horário', 'Limite:', '06:30h - 18:00h')
        expect(@textinformacaominhaconta[0].text).to include('DOC', 'Máximo', 'individual:', 'R$ 4.999,99', 'Horário', 'Limite:', '10:00h - 18:00h')
        # expect(@textinformacaominhaconta[1].text).to include('TED', 'Máximo', 'individual:', 'Ilimitado', 'Horário', 'Limite:', '06:30h - 17:00h')
        expect(@textinformacaominhaconta[1].text).to include('TED', 'Máximo', 'individual:', 'Ilimitado', 'Horário', 'Limite:', '10:00h - 17:00h')
end

Dado('limite para pagamentos') do
        expect(page.has_xpath?("//div[@class='data-row']/h2/strong[text()='Pagamentos']")).to eq true
        expect(@textinformacaominhaconta[2].text).to include('Boleto', 'Máximo', 'individual:', 'R$ 249.999,99', 'Horário', 'Limite:',  '06:30h - 18:00h')

        #mudança update de segurança < 04/03/2021
        # expect(@textinformacaominhaconta[2].text).to include('Boleto', 'Máximo', 'individual:', 'R$ 249.999,99', 'Horário', 'Limite:',  '06:30h - 20:00h')
end

Dado('tributos e convenios') do
        expect(page.has_xpath?("//div[@class='data-row']/h2/strong[text()='Tributos e Convênios']")).to eq true
        expect(@textinformacaominhaconta[3].text).to include('Convênio', 'Máximo', 'individual:', 'Ilimitado', 'Horário Limite:', '06:30h - 15:30h')
end

Entao('verifico os investimentos') do
        expect(page.has_xpath?("//div[@class='data-row']/h2/strong[text()='Investimentos']")).to eq true
        expect(@textinformacaominhaconta[4].text).to include('Máximo', 'individual:', 'Ilimitado', 'Horário Limite:', '00:00h - 23:59h')
end

Dado('que acesso a pagina pacotes de servico') do
        @minhaconta.acessar_pacotes_de_servicos
end

Entao('valido os campos da pagina') do

end

Dado('que acesso a pagina renomear conta') do
        @minhaconta.acessar_renomear_conta
end

Dado('renomeio a conta') do
        #ainda não é possivel alterar o nome da conta, por conta do update por parte da lydians
        # @minhaconta.renomear_conta
end

Entao('eu verifico se a conta foi renomeada') do
        #ainda não implementado
end

Dado('que acesso a pagina trocar senha de acesso') do
        @minhaconta.acessar_trocar_senha_de_acesso
end

Dado('troco a senha de acesso') do
        #ainda não implementado
end

Entao('verifico se a senha foi alterada') do
        #ainda não implementado
end

Dado('que acesso a pagina trocar assinatura eletronica') do
        @minhaconta.acessar_trocar_assinatura_eletronica
end

Dado('respondo a pergunta existente') do
        #ainda não implementado
end

Dado('troco a assinatura eletronica') do
        #ainda não implementado
end

Entao('verifico se a assinatura foi modificada') do
        #ainda não implementado
end

Dado('que acesso a pagina trocar perguntas de seguranca') do
        @minhaconta.acessar_trocar_perguntas_de_seguranca
end

Dado('seleciono a uma questao da lista de questoes') do
        #ainda não implementado
end

Dado('respondo a essa questao') do
        #ainda não implementado
end

Entao('valido se a questao foi alterada') do
        #ainda não implementado
end

Dado('que acesso a pagina informe de rendimentos') do
        @minhaconta.acessar_informe_de_rendimentos
end

Dado('seleciono uma data de informe') do
        #ainda não implementado
end

Entao('verifico os dados exibidos no informe de rendimento') do
        #ainda não implementado
end

Dado('que acesso a pagina gerenciar usuarios') do
        @minhaconta.acessar_gerenciar_usuarios
end

Dado('edito ao usuario com acesso a conta') do
        #ainda não implementado
end

Entao('verifico se as alteracoes foram realizadas') do
        #ainda não implementado
end