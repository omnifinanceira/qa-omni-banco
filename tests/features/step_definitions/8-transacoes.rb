Dado('que verifico transacoes pendentes') do
    home = Home.new
    home.acessar_transacoes

    # click para esperar o resto dos elementos carregarem
    all(:xpath, "//app-last-transactions/div/div/div[3]/div/div[1]/div[7]/label/i")[0].click
    
    textomenutransacoespendentes = all(:xpath, "//div[@class='drop-table -pending']/div[@class='head clearfix']/div", visible: true)
        expect(/#{'Data da solicitação'}/.match(textomenutransacoespendentes[0].text)).to be_truthy
        expect(/#{'Histórico'}/.match(textomenutransacoespendentes[1].text)).to be_truthy
        expect(/#{'Solicitada por'}/.match(textomenutransacoespendentes[2].text)).to be_truthy
        expect(/#{'Valor'}/.match(textomenutransacoespendentes[3].text)).to be_truthy
        expect(/#{'Status'}/.match(textomenutransacoespendentes[4].text)).to be_truthy
        expect(/#{'Data da Efetivação'}/.match(textomenutransacoespendentes[5].text)).to be_truthy
end

Entao('verifico tambem ultimas transacoes solicitadas') do
    textomenuultimastransacoes = all(:xpath, "//div[@class='drop-table -last-transactions']/div[@class='head clearfix']/div", visible: true)
        expect(/#{'Data da solicitação'}/.match(textomenuultimastransacoes[0].text)).to be_truthy
        expect(/#{'Histórico'}/.match(textomenuultimastransacoes[1].text)).to be_truthy
        expect(/#{'Solicitada por'}/.match(textomenuultimastransacoes[2].text)).to be_truthy
        expect(/#{'Valor'}/.match(textomenuultimastransacoes[3].text)).to be_truthy
        expect(/#{'Status'}/.match(textomenuultimastransacoes[4].text)).to be_truthy
        expect(/#{'Data da Efetivação'}/.match(textomenuultimastransacoes[5].text)).to be_truthy

    qtdultimastransacoes = all(:xpath, "//div[@class='body']/div")
        qtdultimastransacoes.count > 0 == true
end