#language:pt
@5 @cobranca
Funcionalidade: Verificar cobranca

Contexto:
    Dado que eu esteja logado

Cenario: verificar pagina de cobranca
    Quando eu acesso a pagina de cobranca
    Entao valido os campos da tabela de sacados

@cadastrarsacadopf
Cenario: Cadastrar sacado pessoa fisica
    Quando eu acesso a pagina de cobranca
    E eu acesso o cadastro de novo sacado
    Entao eu cadastro um sacado pessoa fisica
    E valido se o cadastro foi efetuado
    E clico em emitir boleto
    E preencho valor e data de vencimento
    E informo a assinatura eletronica
    Entao verifico se o boleto foi gerado

@cadastrarsacadopj
Cenario: Cadastrar sacado pessoa juridica
    Quando eu acesso a pagina de cobranca
    E eu acesso o cadastro de novo sacado
    Entao eu cadastro um sacado pessoa juridica
    E valido se o cadastro foi efetuado
    E clico em emitir boleto
    E preencho valor e data de vencimento
    E informo a assinatura eletronica
    Entao verifico se o boleto foi gerado