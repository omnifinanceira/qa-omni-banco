#language:pt
@6 @deposito
Funcionalidade: Realizar deposito por boleto

Contexto:
    Dado que eu esteja logado

Cenario: realizar deposito
    Quando eu acesso a pagina de deposito
    E valido as sugestoes de valor para deposito
    Entao realizo o deposito via boleto
    E valido se o deposito foi efetuado