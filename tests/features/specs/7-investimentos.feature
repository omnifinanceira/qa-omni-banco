#language:pt
@7 @investimentos
Funcionalidade: Validar investimentos

Contexto:
    Dado que eu esteja logado
    E eu acesso a pagina de investimentos

@simulacao
Cenario: Realizar simulacao
    Dado que realizo uma simulacao
    E autentico
    Entao valido se e possivel efetivar o investimento

@extrato
Cenario: Verificar extrato
    Dado que acesso o submenu de extrato
    Entao valido os valores investidos

@porperiodo
Cenario: Verificar investimento por periodo
    Dado que acesso o submenu de por periodo
    Entao valido os valores investidos por periodo

@resgatar
Cenario: Realizar resgate
    Dado que acesso o submenu resgate
    E realizo o resgate
    Entao valido se o resgate foi efetuado