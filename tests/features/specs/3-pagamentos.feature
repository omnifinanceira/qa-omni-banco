#language:pt
@3 @pagamentos
Funcionalidade: Realizar pagamento

Contexto:
    Dado que eu esteja logado

@pagamentoshoje
Cenario: Fazer pagamento hoje
    Quando eu preencho o codigo de barras
    E seleciono a opcao para pagar hoje ou <agendo>
    E autentico
    Entao valido se o pagamento foi efetuado

Exemplos:
|agendo  |
|"false" |
|"true"  |