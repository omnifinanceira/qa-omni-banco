#language:pt
@1 @pagina-inicial
Funcionalidade: Validar campos da pagina inicial

Contexto:
    Dado que eu esteja logado

Cenario: Verificar os campos existentes da pagina inicial
    Quando eu espero visualizar a home do internet banking
    E valido os textos dos campos existentes no menu lateral
    E valido os textos dos campos existentes no balance
    E valido os textos dos campos existentes no lancamento
    E valido o link de evolucao da sua conta e pagamentos de codigo de barras
    Entao valido os textos dos campos existentes nos botoes inferiores