#language:pt
@10 @minha-conta
Funcionalidade: Validar campos da pagina minha conta

Contexto:
    Dado que eu esteja logado
    E acesso a pagina minha conta

@meus-limites
Cenario: Verificar os campos existentes da pagina meus limites
    Dado que verifico o limite para transferencia
    E  limite para pagamentos
    E tributos e convenios
    Entao verifico os investimentos

@pacotes-de-servico
Cenario: Verificar os campos existentes da pagina pacotes de servico
    Dado que acesso a pagina pacotes de servico
    Entao valido os campos da pagina

@renomear-conta
Cenario: Verificar os campos existentes da pagina renomear conta
    Dado que acesso a pagina renomear conta
    E  renomeio a conta
    # E autentico
    Entao eu verifico se a conta foi renomeada

@trocar-senha-de-acesso
Cenario: Verificar os campos existentes da pagina trocar senha de acesso
    Dado que acesso a pagina trocar senha de acesso
    E  troco a senha de acesso
    # E autentico
    Entao verifico se a senha foi alterada

@trocar-assinatura-eletronica
Cenario: Verificar os campos existentes da pagina trocar assinatura eletronica
    Dado que acesso a pagina trocar assinatura eletronica
    E  respondo a pergunta existente
    E troco a assinatura eletronica
    Entao verifico se a assinatura foi modificada

@trocar-perguntas-de-seguranca
Cenario: Verificar os campos existentes da pagina trocar perguntas de seguranca
    Dado que acesso a pagina trocar perguntas de seguranca
    E seleciono a uma questao da lista de questoes
    E respondo a essa questao
    Entao valido se a questao foi alterada

@informe-de-rendimentos
Cenario: Verificar os campos existentes da pagina informe de rendimentos
    Dado que acesso a pagina informe de rendimentos
    E  seleciono uma data de informe
    Entao verifico os dados exibidos no informe de rendimento

@gerenciar-usuarios
Cenario: Verificar os campos existentes da pagina gerenciar usuarios
    Dado que acesso a pagina gerenciar usuarios
    E edito ao usuario com acesso a conta
    Entao verifico se as alteracoes foram realizadas