#language:pt
@0 @login
Funcionalidade: Login no aplicativo

@login-sucesso
Cenario: Quando eu realizo o login com sucesso

Quando eu preencher o campo <cpf>
    E eu clicar no botao proximo
    E eu digitar a <senha>
    E eu clicar no botao Entrar
    Entao eu espero visualizar a home do internet banking
    
Exemplos:
    |cpf          |senha   |
    |"71707649030"|"140592"|

@login-falha
Cenario: Quando eu realizo o login com cpf nao cadastrado

Quando eu preencher o campo <cpf>
    E eu clicar no botao proximo
    Entao verifico a <mensagem> de retorno

Exemplos:
    |cpf          |mensagem            |
    |"01000150000"|"Não foi possível consultar as credenciais do usuário. Por favor tente novamente mais tarde. "|

@login-senha-falha
Cenario: Quando eu realizo o login com senha invalida

Quando eu preencher o campo <cpf>
    E eu clicar no botao proximo
    E eu digitar a <senha>
    E eu clicar no botao Entrar
    Entao verifico a <mensagem> de retorno

Exemplos:
    |cpf          |senha   |mensagem          |
    |"71707649030"|"111111"|"Não foi possível autenticar, usuário ou senha inválido. "|