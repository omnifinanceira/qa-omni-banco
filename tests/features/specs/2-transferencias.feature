#language:pt
@2 @transferencias
Funcionalidade: Realizar transferencias

Contexto:
    Dado que eu esteja logado
    E acesso a transferencias
  
@transferenciaoutrobancodoc
Cenario: Realizar transferencias para outros bancos por doc
    E seleciono o tipo de transferencia "DOC"
    E preencho os dados do favorecido <banco>,  <modalidade>, <agencia>, <conta>, <titularidade> e <favorecido>
    E preencho os dados da transferencia <valor> e <agendamento>
    Entao valido as informacoes a serem efetivadas <banco>,  <modalidade>, <agencia>, <conta> e  <titularidade>
    E autentico
    Entao valido se a transferencia foi realizada

Exemplos:
|banco                     |modalidade      |agencia|conta   |titularidade        |valor |agendamento|favorecido|
# |"341 - itau unibanco s.a."|"Conta Corrente"|"7867" |"187877"|"Mesma Titularidade"|"1000"|"false"    |"false"   |
# |"341 - itau unibanco s.a."|"Conta Corrente"|"7867" |"187877"|"Mesma Titularidade"|"1000"|"true"     |"false"   |

@transferenciaoutrobancoted
Cenario: Realizar transferencias para outros bancos por ted 
    E seleciono o tipo de transferencia "TED"
    E preencho os dados do favorecido <banco>,  <modalidade>, <agencia>, <conta>, <titularidade> e <favorecido>
    E preencho os dados da transferencia <valor> e <agendamento>
    Entao valido as informacoes a serem efetivadas <banco>,  <modalidade>, <agencia>, <conta> e  <titularidade>
    E autentico
    Entao valido se a transferencia foi realizada

Exemplos:
|banco                     |modalidade      |agencia|conta   |titularidade        |valor |agendamento|favorecido|
# |"341 - itau unibanco s.a."|"Conta Corrente"|"7867" |"187877"|"Mesma Titularidade"|"1000"|"false"    |"false"   |
# |"341 - itau unibanco s.a."|"Conta Corrente"|"7867" |"187877"|"Mesma Titularidade"|"1000"|"true"     |"false"   |

@transferenciaentrecontaomni
Cenario: Realizar transferencia entre conta omni
    Quando eu transfiro entre <conta> omni
    E preencho os dados da transferencia <valor> e <agendamento>
    E autentico
    Entao valido se a transferencia entre conta omni foi realizada

Exemplos:
|conta  |valor |agendamento|
# |"49704"|"1000"|"false"    |
# |"49704"|"1000"|"true"     |

@transferenciaadicionarfavorecido
Cenario: Realizar transferencias para outros bancos por doc
    Quando acesso favorecidos
    E clico em adicionar favorecido
    E preencho os dados do favorecido <banco>,  <modalidade>, <agencia>, <conta>, <titularidade> e <favorecido>
    E autentico
    Entao valido se o favorecido foi adicionado

Exemplos:
|banco                     |modalidade      |agencia|conta   |titularidade        |favorecido|
|"341 - itau unibanco s.a."|"Conta Corrente"|"7867" |"187877"|"Mesma Titularidade"|"true"    |
# |"341 - itau unibanco s.a."|"Conta Corrente"|"7867" |"187877"|"Outra Titularidade"|"true"    |