class Token < Util
  include Appium::Core

  element :elementinputassinaturaeletronica, :xpath, "//input[@formcontrolname='eletronicSignature']"
  element :elementinputtoken, :xpath, "//input[@formcontrolname='token']"
  element :elementbuttoncontinuar, :xpath, "//button/span/span[text()='Continuar']"
  element :elementbuttonfinalizar, :xpath, "//button/span/span[text()='Finalizar']"
  element :elementbuttonproximo, :xpath, "//button/span/span[text()='Próximo']"

  def initialize
    @arquivo = Dir.glob('../../token/tests/token.txt').first
  end

  def acessar_mobile
    Wait.until { find_element(id: "br.com.omni.omni:id/etCpf").send_keys($cpf) }
    Wait.until { find_element(id: "br.com.omni.omni:id/btnNext").click }
    Wait.until { find_element(id: "br.com.omni.omni:id/etPassword") }
    Wait.until { find_element(id: "br.com.omni.omni:id/btnToken") }
  end
  
  def clicar_token
    Wait.until { find_element(id: "br.com.omni.omni:id/btnToken").click }
  end

  def pegar_token
    tokentext = ""
    Wait.until { tokentext = find_element(id: "br.com.omni.omni:id/tvTokenNumber").text.gsub(" ", "") }
    return tokentext
  end

  def pegar_token_txt
    arquivoaberto = File.open(@arquivo)
    tokentext = arquivoaberto.read

    return tokentext.split("\n")[0]
  end

  def preencher_assinatura_eletronica(senha = $assinaturaeletronica)
    elementinputassinaturaeletronica.visible?
    elementinputassinaturaeletronica.click
    digitos = senha.split("")
    digitos.each do |digito|
      find(".keyboard-button", text: digito).click
    end
  end

  def preencher_token(token)
    elementinputtoken.visible?
    elementinputtoken.click

    digitos = token.split("")
    digitos.each do |digito|
      find(".keyboard-button", text: digito).click
    end
  end

  def clicar_proximo
    elementbuttonproximo.click
  end

  def clicar_finalizar
    elementbuttonfinalizar.click
  end
end
