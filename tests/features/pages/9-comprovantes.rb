class Comprovantes < SitePrism::Page

    set_url ''

    element :elementbuttonhoje, :xpath, "//div[@data-component='special-filter']/div[@class='filter-holder']/div/button/span[text()='Hoje']"
    element :elementbutton15dias, :xpath, "//div[@data-component='special-filter']/div[@class='filter-holder']/div/button/span[text()='15 dias']"
    element :elementbutton30dias, :xpath, "//div[@data-component='special-filter']/div[@class='filter-holder']/div/button/span[text()='30 dias']"
    element :elementbuttonperiodo, :xpath, "//div[@data-component='special-filter']/div[@class='filter-holder']/div/button/span[text()='Período']"

    def hoje
        elementbuttonhoje.click
    end

    def quinze_dias
        elementbutton15dias.click
    end

    def trinta_dias
        elementbutton30dias.click
    end

    def periodo
        elementbuttonperiodo.click
    end
end