require_relative 'geradorrandomico.rb'

class Investimentos < GeradorRandomico

    element :elementlinksimulacao, :xpath, "//a[@href='/investimentos/simulacao']"
    element :elementlinkextrato, :xpath, "//a[@href='/investimentos/extrato']"
    element :elementlinkporperiodo, :xpath, "//a[@href='/investimentos/por-periodo']"
    element :elementlinkresgatar, :xpath, "//a[@href='/investimentos/resgatar-saldo']"
    element :elementinputvalor, :xpath, "//div/input[@formcontrolname='investValue']"
    element :elementbuttoncontinuar, :xpath, "//button/span/span[text()='Continuar']"
    element :elementbuttoninvestir, :xpath, "//button/span/span[text()='Investir']"

    def acessar_simulacao
        elementlinksimulacao.click
    end

    def acessar_extrato
        elementlinkextrato.click
    end

    def acessar_por_periodo
        elementlinkporperiodo.click
    end

    def acessar_resgatar
        elementlinkresgatar.click
    end

    def realizar_simulacao
        elementinputvalor.set(gerar_numero(1000).to_s + ',00')
        elementbuttoncontinuar.click(5)
        elementbuttoninvestir.click
    end
end