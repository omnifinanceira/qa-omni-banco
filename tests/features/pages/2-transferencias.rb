require_relative 'geradorrandomico.rb'

class Transferencias < Util

    set_url ''

    element :elementlinkoutrosbancos, :xpath, "//li[@class='item-submenu']/a[@href='/transferencias/transferencias-outros-bancos']"
    element :elementlinkentrecontas, :xpath, "//li[@class='item-submenu']/a[@href='/transferencias/transferencias-entre-contas']"
    element :elementlinkfavorecidos, :xpath, "//li[@class='item-submenu']/a[@href='/transferencias/favorecidos']"

    element :elementbuttonconcluir, :xpath, "//div/button/span/span[text()='Continuar']"
    element :elementbuttoncancelar, :xpath, "//div/button/span/span[text()='Cancelar']"
    element :elementinputnomebanco, :xpath, "//input[@formcontrolname='bank']"
    element :elementcheckboxmodalidade, :xpath, "//mat-select[@formcontrolname='modality']"
    element :elementinputagencia, :xpath, "//input[@formcontrolname='destinationAgency']"
    element :elementinputagenciaoff, :xpath, "//input[@formcontrolname='agency']"
    element :elementinputconta, :xpath, "//input[@formcontrolname='destinationAccount']"
    element :elementinputcontaoff, :xpath, "//input[@formcontrolname='account']"
    element :elementcheckboxtitularidade, :xpath, "//mat-select[@formcontrolname='ownership']"
    element :elementinputnameoutratitularidade, :xpath, "//input[@formcontrolname='name']"
    element :elementinputcpfoutratitularidade, :xpath, "//input[@formcontrolname='cpfCnpj']"
    element :elementinputvalor, :xpath, "//input[@formcontrolname='value']"
    element :elementcheckhoje, :xpath, "//mat-radio-group[@formcontrolname='transferDate']/div/mat-radio-button[@value='today']" 
    element :elementcheckdata, :xpath, "//mat-radio-group[@formcontrolname='transferDate']/div/mat-radio-button[@value='schedule']", wait: 5
    element :elementdivcalendario, :xpath, "//button['datepicker-calendar material-icons'][text()='event']", wait: 5
    element :elementinputassinaturaeletronica, :xpath, "//input[@formcontrolname='eletronicSignature']"
    element :elementinputtoken, :xpath, "//input[@formcontrolname='token']"
    element :elementbuttonadicionarfavorecido, :xpath, "//button[text()='Adicionar favorecido']"
    element :elementinputsalvarfavorecido, :xpath, "//label/div[@class='mat-checkbox-inner-container']"
    element :elementbuttoncontinuar, :xpath, "//button/span/span[text()='Continuar']"
    element :elementmodalavisoperiodo, :xpath, "//div[@class='modal container']/h2[text()='Aviso!']"
    element :elementbuttonavisosim, :xpath, "//button/span/span[text()='Sim']"

    def initialize
        @gerador = GeradorRandomico.new
    end

    def acessar_outros_bancos
        elementlinkoutrosbancos.click
    end
    
    def acessar_entre_bancos
        elementlinkentrecontas.click
    end

    def acessar_favorecidos
        elementlinkfavorecidos.click
    end
    
    def selecionar_tipo_transferencia(tipotransferencia)
        find(:xpath, "//div[@class='mat-radio-label-content'][text()='#{tipotransferencia}']").click
        elementbuttonconcluir.click
    end

    def adicionar_agencia_e_conta(agencia,conta,adcfavorecido)
        if adcfavorecido
            elementinputagenciaoff.set(@gerador.gerar_numero(1111,9999))
            elementinputcontaoff.set(@gerador.gerar_numero(111111,999999))
        else
            elementinputagencia.set(agencia)
            elementinputconta.set(conta)
        end
    end

    def preencher_dados_favorecidos(banco,modalidade,agencia,conta,titularidade,adcfavorecido)
        elementinputnomebanco.click
        find(:xpath, "//mat-option/span", text: banco).click
        elementcheckboxmodalidade.click
        find(:xpath, "//mat-option/span", text: modalidade).click
        adicionar_agencia_e_conta(agencia,conta,adcfavorecido)
        elementcheckboxtitularidade.click
        find(:xpath, "//mat-option/span", text: titularidade).click

        if titularidade == 'Outra Titularidade'
            elementinputnameoutratitularidade.set(@gerador.gerar_nome)
            elementinputcpfoutratitularidade.set(@gerador.gerar_cpf)
        end

        elementbuttoncontinuar.click
    end

    def clicar_continuar
        elementbuttoncontinuar.click

        begin
            if find(:xpath, "//div[@class='modal container']/h2[text()='Aviso!']").visible?
                elementbuttonavisosim.click
            end

          rescue Exception => ex
            puts "Antes das 17h00 " + ex.message
        end
    end

    def preencher_valor(valor)
        valoraleatorio = @gerador.gerar_numero(9,3999)
        valortotal = valor.to_i + valoraleatorio.to_i
        elementinputvalor.set(valortotal.to_s + "00")
        return valortotal.to_s
    end

    def selecionar_data(agendar)
        if agendar
            data = Util.new
            dia = data.data_util

            elementcheckdata.click
            
            elementdivcalendario.click
            find(:xpath, "//div[@class='mat-calendar-body-cell-content'][text()='#{dia}']").click

            elementbuttoncontinuar.click
        else
            elementcheckhoje.click
            clicar_continuar
        end
    end

    def adicionar_favorecido
        elementbuttonadicionarfavorecido.click
    end

    def selecionar_favorecido
        #preencher os campos ainda não mapeado
        elementbuttoncontinuar.click
    end

    def transferir_entre_contas(conta,salvarfavorecido)
        elementinputconta.set(conta)

        if salvarfavorecido == true
            elementinputsalvarfavorecido.click
        end

        elementbuttonconcluir.click
    end
end