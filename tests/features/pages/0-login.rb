require_relative 'util.rb'
class Login < Util

    set_url '/login'

    element :elementinputcpf, :xpath, "//input[@formcontrolname='cpf']"
    element :elementbuttonproximo, :xpath, "//button/span/span[text()='Próximo']"
    element :elementbuttonentrar, :xpath, "//button/span/span[text()='Entrar']"
    
    def preencher_cpf(cpf = $cpf)
        elementinputcpf.set cpf
    end

    def clicar_botao_proximo
        elementbuttonproximo.click
    end

    def preencher_senha(senha = $senha)
        digitos = senha.split("")

        digitos.each do |digito|
          find(".keyboard-button", text: digito).click
        end
    end

    def acessar_conta
        elementbuttonentrar.click
    end

    def acessar_conta_ib
        preencher_cpf($cpf)
        clicar_botao_proximo
        preencher_senha($senha)
        acessar_conta
    end
end