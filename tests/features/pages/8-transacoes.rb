class Transacoes < SitePrism::Page

    set_url ''

    element :elementbuttoncancelar, :xpath, "//button[@class='btn border primary mat-raised-button mat-primary']/span/span[text()='Cancelar']"
    
    #validar se o seletor do prosseguir está correto !
    element :elementbuttonprosseguir, :xpath, "//button[@class='btn border primary mat-raised-button mat-primary']/span/span[text()='Prosseguir']"

    def cancelar
        elementbuttoncancelar.click
    end

    def prosseguir
        elementbuttonprosseguir.click
    end
end