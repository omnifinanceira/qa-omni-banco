class Home < SitePrism::Page

    set_url ''

    element :elementbuttonpaginainicial, :xpath, "//a[@href='/']/span[text()='Página inicial']"
    element :elementbuttontransferencias, :xpath, "//a[@href='/transferencias']"
    element :elementbuttonpagamentos, :xpath, "//a[@href='/pagamentos']"
    element :elementbuttonextratodeconta, :xpath, "//a[@href='/extrato']"
    element :elementbuttoncobranca, :xpath, "//a[@href='/cobranca']"
    element :elementbuttondeposito, :xpath, "//a[@href='/depositos']"
    element :elementbuttoninvestimentos, :xpath, "//a[@href='/investimentos']"
    element :elementbuttontransacoes, :xpath, "//a[@href='/transacoes']"
    element :elementbuttoncomprovantes, :xpath, "//a[@class='item' and @href='/comprovantes']"
    element :elementbuttonminhaconta, :xpath, "//a[@href='/minha-conta']"
    element :elementbuttonsair, :xpath, "//button[@title='Sair']"

    def acessar_pagina_inicial
        elementbuttonpaginainicial.click
    end

    def acessar_transferencias
        elementbuttontransferencias.click
    end

    def acessar_pagamentos
        elementbuttonpagamentos.click
    end

    def acessar_extrato_de_conta
        elementbuttonextratodeconta.click
    end

    def acessar_cobranca
        elementbuttoncobranca.click
    end

    def acessar_deposito
        elementbuttondeposito.click
    end

    def acessar_investimentos
        elementbuttoninvestimentos.click
    end

    def acessar_transacoes
        elementbuttontransacoes.click
    end

    def acessar_comprovantes
        elementbuttoncomprovantes.click
    end

    def acessar_minha_conta
        elementbuttonminhaconta.click
    end

    def sair
        elementbuttonsair.click
    end
end