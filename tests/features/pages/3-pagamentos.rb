class Pagamentos < SitePrism::Page

    set_url ''

    element :elementinputcodigodebarras, :xpath, "//div/input[@formcontrolname='barCode']"
    element :elementcheckhoje, :xpath, "//mat-radio-button[@value='today']/label/div[@class='mat-radio-container']"
    element :elementcheckdataagendamento, :xpath, "//mat-radio-button[@value='schedule']"
    element :elementdivcalendario, :xpath, "//div/button[@class='datepicker-calendar material-icons'][text()='event' and not(@hidden)]"
    element :elementbuttoncontinuar, :xpath, "//button/span/span[text()='Continuar']"
    
    def preencher_codigo_de_barras(codigodebarra)
        elementinputcodigodebarras.set (codigodebarra)
        elementinputcodigodebarras.send_keys :tab

        begin
            while find(:xpath, "//div[@id='loader-wrapper']")
            end
        rescue
        end
    end
    
    def clicar_agendamento
        elementcheckdataagendamento.click
    end

    def selecionar_data(agendar)
        if agendar
            data = Util.new
            dia = data.data_util

            elementcheckdataagendamento.click
            
            elementdivcalendario.click
            find(:xpath, "//div[@class='mat-calendar-body-cell-content'][text()='#{dia}']").click

            elementbuttoncontinuar.click
        else
            elementcheckhoje.click
            elementbuttoncontinuar.click
        end
    end

    def clicar_continuar
        elementbuttoncontinuar.click
    end
end
