class ExtratoDeConta < SitePrism::Page

    set_url ''

    element :elementlinkhoje, :xpath, "//li/a[@href='/extrato/hoje']"
    element :elementlink15dias, :xpath, "//li/a[@href='/extrato/15-dias']"
    element :elementlink30dias, :xpath, "//li/a[@href='/extrato/30-dias']"
    
    def acessar_extrato_hoje
        elementlinkhoje.click
    end

    def acessar_extrato_15dias
        elementlink15dias.click
    end

    def acessar_extrato_30dias
        elementlink30dias.click
    end
end