class Util < SitePrism::Page

    def initialize
        $cpf = "71707649030"
        $senha = "140592"
        $assinaturaeletronica = "140593"
        criar_pasta_log
    end

    def data_util
        data = Time.now
        if data.friday?
            datareturn = data.day + 3
        elsif data.saturday?
            datareturn = data.day + 2
        else
            datareturn = data.day + 1
        end
        
        return datareturn
    end

    def pegar_codigo_de_barras
        codigodebarras = ""
        arquivo = SimpleXlsxReader.open(Dir.glob('../../ref-omni-banco/tests/dados/*.xlsx').first)
        planilha = arquivo.sheets.first
        
        planilha.data.each do |row|
            codigodebarras = row[0]
            break
        end
        return codigodebarras
    end

    def apagar_codigo_de_barras
        arquivo = RubyXL::Parser.parse(Dir.glob('../../ref-omni-banco/tests/dados/*.xlsx').first)
        planilha = arquivo[0]
        planilha.delete_row(1)
        arquivo.write(Dir.glob('../../ref-omni-banco/tests/dados/*.xlsx').first)
    end

    def criar_pasta_log
        @diretorio = "C:/report_automacao"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

        @diretorio = "#{Dir.pwd}/reports"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
    end
end