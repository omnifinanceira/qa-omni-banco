require_relative 'geradorrandomico.rb'

class Cobranca < GeradorRandomico

    element :elementbuttoncadastrarsacado, :xpath, "//button/span[text()='Cadastrar sacado']"
    element :elementcheckpessoajuridica, :xpath, "//label/div[text()='Pessoa Jurídica']"
    element :elementinputcpfcnpj, :xpath, "//input[@formcontrolname='cpfCnpj']"
    element :elementinputnome, :xpath, "//input[@formcontrolname='nome']"
    element :elementinputemail, :xpath, "//input[@formcontrolname='email']"
    element :elementinputcep, :xpath, "//input[@formcontrolname='cep']"
    element :elementinputnumerocasa, :xpath, "//input[@formcontrolname='numero']"
    element :elementbuttonconfirmar, :xpath, "//button[@class='btn' and @type='submit']"
    element :elementbuttonemitirboleto, :xpath, "//button/span[text()='Emitir Boleto']"
    element :elementinputvalorboleto, :xpath, "//input[@formcontrolname='value']"
    element :elementdivcalendario, :xpath, "//button['datepicker-calendar material-icons'][text()='event']", wait: 5
    element :elementbuttonconfirmarboleto, :xpath, "//button/span[text()='Confirmar']"
    element :elementbuttongerarboleto, :xpath, "//button/span[text()='Gerar boleto']"

    def acessar_sacado
        elementbuttoncadastrarsacado.click
    end

    def cadastrar_sacado(pj = false)

        if pj == true
            elementcheckpessoajuridica.click
            elementinputcpfcnpj.set(gerar_cnpj)
        else
            elementinputcpfcnpj.set(gerar_cpf)
        end

        elementinputnome.set(gerar_nome)
        elementinputemail.set(gerar_email)
        elementinputcep.set(gerar_cep)
        elementinputnumerocasa.set(gerar_numero)

        elementbuttonconfirmar.click(5)
        if (!(find(:xpath, "//div/app-warning/div/p").visible?))
            elementbuttonconfirmar.click
        end
    end

    def clicar_emitir_boleto
        elementbuttonemitirboleto.click
    end

    def preencher_valor
        valor = gerar_numero(20,9990).to_s + ',00'
        elementinputvalorboleto.set(valor)
        return valor
    end

    def selecionar_data
        data = Util.new
        dia = data.data_util

        elementdivcalendario.click
        find(:xpath, "//div[@class='mat-calendar-body-cell-content'][text()='#{dia}']").click(wait: 5)
        return dia
    end

    def clicar_confirmar
        elementbuttonconfirmarboleto.click
    end

    def clicar_gerar_boleto
        elementbuttongerarboleto.click
    end
end