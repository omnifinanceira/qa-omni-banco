require_relative 'geradorrandomico.rb'

class Deposito < GeradorRandomico

    element :elementinputvalor, :xpath, "//input[@formcontrolname='value']"
    element :elementbuttoncontinuar, :xpath, "//button/span/span[text()='Continuar']"

    def realizar_deposito
        elementinputvalor.set(gerar_numero(20,9990).to_s + ',00')
        elementbuttoncontinuar.click
    end
end