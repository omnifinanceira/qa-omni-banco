class PaginaInicial < SitePrism::Page

    set_url ''

    element :elementbuttonverextratocompleto, :xpath, "//a[@href='/extrato/30-dias' and @class='btn border non-printable btnSecondary']"
    element :elementbuttonminhacontarenomear, :xpath, "//a[@href='/minha-conta/renomear-conta' and @class='block']"
    element :elementbuttontranferenciasoutrosbancos, :xpath, "//a[@href='/transferencias/transferencias-outros-bancos' and @class='block']"
    element :elementbuttoninvestimentossimulacao, :xpath, "//a[@href='/investimentos/simulacao' and @class='block']"
    element :elementbuttoncomprovantes, :xpath, "//a[@href='/comprovantes' and @class='block']"

    def acessar_extrato_completo
        elementbuttonverextratocompleto.click
    end

    def acessar_minha_conta_renomear
        elementbuttonminhacontarenomear.click
    end

    def acessar_transferencias_outros_bancos
        elementbuttontranferenciasoutrosbancos.click
    end

    def acessar_investimentos_simulacao
        elementbuttoninvestimentossimulacao.click
    end

    def acessar_comprovantes
        elementbuttoncomprovantes.click
    end
end