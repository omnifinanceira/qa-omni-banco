class MinhaConta < SitePrism::Page

    set_url ''

    element :elementlinkmeuslimites, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/meus-limites']"
    element :elementlinkpacotesdeservicos, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/meu-pacote']"
    element :elementlinkrenomearconta, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/renomear-conta']"
    element :elementinputrenomearconta, :xpath, "//input[@formcontrolname='accountName']"
    element :elementlinktrocarsenhadeacesso, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/redefinir-senha']"
    element :elementlinktrocarassinaturaeletronica, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/redefinir-assinatura-eletronica']"
    element :elementlinktrocarperguntasdeseguranca, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/redefinir-perguntas-de-seguranca']"
    element :elementlinkinformederendimentos, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/informe-de-rendimentos']"
    element :elementlinkgerenciasusuarios, :xpath, "//ul[@class='list-submenu active']/li[@class='item-submenu']/a[@routerlink='/minha-conta/gerenciar-usuarios']"

    def acessar_meus_limites
        elementlinkmeuslimites.click
    end

    def acessar_pacotes_de_servicos
        elementlinkpacotesdeservicos.click
    end

    def acessar_renomear_conta
        elementlinkrenomearconta.click
    end

    def renomear_conta
        nome = GeradorRandomico.new
        elementinputrenomearconta.set(nome.gerar_nome)
    end

    def acessar_trocar_senha_de_acesso
        elementlinktrocarsenhadeacesso.click
    end

    def acessar_trocar_assinatura_eletronica
        elementlinktrocarassinaturaeletronica.click
    end

    def acessar_trocar_perguntas_de_seguranca
        elementlinktrocarperguntasdeseguranca.click
    end

    def acessar_informe_de_rendimentos
        elementlinkinformederendimentos.click
    end

    def acessar_gerenciar_usuarios
        elementlinkgerenciasusuarios.click
    end
end